# FFMPEG Bitstream Analysis Examples

This repository includes four examples for doing bitstream analysis using FFMPEG. They are written in C++20 as much as possible. Note that FFMPEG was not designed for bitstream analysis so while it is an obvious API choice, it is often inefficient and for many codecs it is impossible to get full bitstream information. In particular, there is no support on FFMPEG for transform coefficients. This code is written for FFMPEG 4.3.2. Note that FFMPEG frequently undergoes breaking API changes, so it is likely that these examples will not work forever. All examples are confirmed working with short MPEG-4 part 2 encoded videos, YMMV with other codecs.

FFMPEG does provide an API for retrieving packets from containers (which is part of the standard decoding process), thereby abstracting container parsing. This API could be used to build a container-agnostic bitstream analysis library however packet data (NALs) would need to be parsed manually. I currently don't have an example of this.

The examples are:

* `full_decode`: Decodes a video using the standard FFMPEG process and writes the RGB frames to disk as PPM files
* `save_motion`: Partially decodes the video by writing the YCbCr frames to disk for I-frames (3 PGM files) and the motion vectors for P-frames to CSV files. 
* `decode_with_motion`: Partially decodes a video by decoding I-frames to RGB images and saving them to disk, then applying motion vectors for P-frames to the previously decoded RGB frame. Note that this is not a correct frame since it does not use residuals.
* `save_motion_and_residual`: Partially decodes a video by decoding I-frames to RGB, computing residuals for P-frames by fully decoding them and subtracting from a version of the frame with applied motion vectors and saving the residuals and motion vectors to disk.
