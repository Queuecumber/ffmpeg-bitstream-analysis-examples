/**
 * This example uses ffmpeg to parse the container format and to decode the resulting bitstream. 
 * For I-frames the decoded image is saved in its native format with 4:2:0 chroma subsampling.
 * The image is saved in three separate PGM files, one for each channel. For P-frames, motion vectors written to csv files. 
 * This code assumes that the underlying codec:
 *  (a) Uses full-frames, i.e., it does not use slices
 *  (b) The FFMPEG implementation supports exporting motion vectors 
 *  (c) Uses the yuv420p format for I-frames
 */

// FFMPEG headers are pure C, they need to be surrounded by extern "C" to avoid name mangling
extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/motion_vector.h>
#include <libswscale/swscale.h>
}

#include <array>
#include <fstream>
#include <iostream>
#include <sstream>

void write_pgm(const std::string &fname, AVFrame *frame, int channel = 0, int w = -1, int h = -1) {
    // Compared to the ppm routine in full_decode, we need to allow passing the channel number for
    // the planar format and overriding the width and height for chroma subsampling
    // Note that PGM uses P5 as its magic number, this is important
    if (w < 0)
        w = frame->width;

    if (h < 0)
        h = frame->height;

    // PGM file format:
    // Line 1: format identifier P5
    // Line 2: width <space> height <space> max_value (255)
    // Line 3-end: raw grayscale image data
    std::ofstream output_image;
    output_image.open(fname, std::ios::binary | std::ios::out);
    output_image << "P5\n"
                 << w << " " << h << "\n255\n";

    for (int y = 0; y < frame->height; y++) {
        output_image.write((const char *)frame->data[channel] + (y * frame->linesize[channel]), frame->width);
    }
}

int main(int argc, char **argv) {
    // Read the container and set up the streams and codecs, see full_decode for more details
    AVFormatContext *format_context = nullptr;

    if (avformat_open_input(&format_context, argv[1], nullptr, nullptr) != 0) {
        std::cout << "Unable to open " << argv[1] << std::endl;
        return -1;
    }

    if (avformat_find_stream_info(format_context, nullptr) < 0) {
        std::cout << "Unable to parse video streams, video file is invalid" << std::endl;
    }

    av_dump_format(format_context, 0, argv[1], 0);

    AVCodec *codec = nullptr;
    int stream = av_find_best_stream(format_context, AVMEDIA_TYPE_VIDEO, -1, -1, &codec, 0);

    if (stream < 0) {
        std::cout << "Unable to find a video stream in the file" << std::endl;
        return -1;
    }

    AVCodecParameters *codec_params = format_context->streams[stream]->codecpar;

    AVCodecContext *codec_context = avcodec_alloc_context3(codec);
    avcodec_parameters_to_context(codec_context, codec_params);

    // Before we open the codec, we ask it to export motion vectors with this flag
    AVDictionary *opts = nullptr;
    av_dict_set(&opts, "flags2", "+export_mvs", 0);

    if (avcodec_open2(codec_context, codec, &opts) < 0) {
        std::cout << "Unable to load codec" << std::endl;
        return -1;
    }

    // We're only going to deal with the frame as extracted from the stream, so no RGB conversion
    AVFrame *nativeFrame = av_frame_alloc();

    // Parse the stream
    int framenum = 0;
    for (AVPacket packet; av_read_frame(format_context, &packet) >= 0; framenum++) {
        if (packet.stream_index == stream) {
            // Unfortunately the frame still needs to be fully decoded to get motion vectors
            avcodec_send_packet(codec_context, &packet);
            avcodec_receive_frame(codec_context, nativeFrame);

            if (nativeFrame->pict_type == AV_PICTURE_TYPE_I) {
                // I-frame
                std::array<std::string, 3> channel_names{"Y", "Cb", "Cr"};

                // Pull the three channels out and write them to separate PGM files
                // PGM files are grayscale, so they will be storing a single channel of the color image
                // The video frame is assumed to be what FFMPEG calls YUV420p
                // that means:
                // YCbCr (erroneously called YUV)
                // 4:2:0 chroma subsampling (chroma channels have half the width and height of the luma channel)
                // planar format, i.e., the entire luma channel is stored then the entire Cb channel etc. in contrast to the interlaced RGB format we had in full_decode
                for (int c = 0; c < 3; c++) {
                    float size_multiplier = c > 0 ? 0.5 : 1; // 4:2:0 chroma subsampling means we need to halve the width and height of color channels
                    int h = int(nativeFrame->height * size_multiplier);
                    int w = int(nativeFrame->width * size_multiplier);

                    // Other than that we can write the frame in the same way as full_decode, but with some more parameters
                    std::ostringstream fname_format;
                    fname_format << "frame" << framenum << "_" << channel_names[c] << ".pgm";
                    write_pgm(fname_format.str(), nativeFrame, c, w, h);
                }
            } else {
                // P-frame
                // Pull the motion vectors out of the frame side data
                AVFrameSideData *sd = av_frame_get_side_data(nativeFrame, AV_FRAME_DATA_MOTION_VECTORS);
                AVMotionVector *mvs = reinterpret_cast<AVMotionVector *>(sd->data);
                int n = sd->size / sizeof(AVMotionVector); // compute the number of motion vectors

                // Store them in a CSV file
                std::ostringstream fname_format;
                fname_format << "frame" << framenum << "_motion.csv";

                std::ofstream output_motion;
                output_motion.open(fname_format.str());
                output_motion << "source,width,height,src_x,src_y,dst_x,dst_y,motion_x,motion_y,motion_scale" << std::endl;

                // This is essentially just dumping the format FFMPEG uses to store motion vectors
                // They store the source frame, the size of the block, the src and destination locations, and the amount of motion in a rational format (not float)
                for (auto it = mvs; it != mvs + n; it++) {
                    output_motion << it->source << "," << int(it->w) << "," << int(it->h) << "," << it->src_x << "," << it->src_y << "," << it->dst_x << "," << it->dst_y << "," << it->motion_x << "," << it->motion_y << "," << it->motion_y << std::endl;
                }
            }
        }

        // free the packet
        av_packet_unref(&packet);
    }

    // cleanup everything else
    av_frame_free(&nativeFrame);

    avcodec_close(codec_context);
    avcodec_free_context(&codec_context);
    avformat_close_input(&format_context);
    avformat_free_context(format_context);

    return 0;
}